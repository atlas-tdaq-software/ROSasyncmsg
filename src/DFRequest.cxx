#include <thread>
#include <chrono>

#include "DFRequest.h"
#include "ROSasyncmsg/DataServer.h"
#include "RosDataOutputMsg.h"
#include "ROSEventFragment/EventFragment.h"
#include "ROSIO/ROSFragmentBuilder.h"
#include "ROSCore/DataChannel.h"
#include "DFDebug/DFDebug.h"

#include "ROSCore/CoreException.h"
#include "ROSIO/IOException.h"

#include "ROSUtilities/ROSErrorReporting.h"

using namespace ROS;

ROSFragmentBuilder* DFRequest::s_rosBuilder=0;

void DFRequest::configure(unsigned int detectorId) {
   s_rosBuilder=new ROSFragmentBuilder;
}


DFRequest::DFRequest(
//   std::unique_ptr<std::vector<std::uint32_t>> rols,
   std::vector<std::uint32_t>* rols,
   DataServerSession* session,
   std::uint32_t level1Id,
   std::uint32_t transactionId)
   :  DataRequest(level1Id,0),
      m_session(session),
      m_transactionId(transactionId) {

   if (rols->size()!=0) {
//         std::cout << "Received RoI request\n";
         m_dataChannels.reset(new std::vector<DataChannel*>());
         for (auto rolId : *rols) {
            try {
               m_dataChannels->push_back(DataChannel::channel(rolId));
            }
            catch  (CoreException &e) {
               if (e.getErrorId() == CoreException::UNDEFINEDID) {
                  ENCAPSULATE_ROS_EXCEPTION(newExecption, IOException, IOException::BAD_DATA_REQUEST, e,
                                            " level1Id=" << level1Id 
                                            << " rolId=" << std::hex << rolId);
                  ers::error(newExecption);
               }
               else {
                  throw;
               }
            }
         }
         m_builder=s_rosBuilder;
         m_chanStartIter=m_dataChannels->begin();
         m_nChannels=m_dataChannels->size();
   }
   else {
//      std::cout << "Received EB request\n";
      m_builder=s_rosBuilder;
      m_chanStartIter=DataChannel::channels()->begin();
      m_nChannels=DataChannel::channels()->size();
   }
}


DFRequest::~DFRequest(){
}


int DFRequest::execute() {
   {
//      std::lock_guard<std::mutex> lock(*s_mutex);
      s_mutex->lock();
      if (s_pool->numberOfFreePages()>2) {
         // Ask the builder to create us an empty fragment.
         DEBUG_TEXT(DFDB_ROSCORE, 20, "DFRequest::execute calling builder->createFragment for L1Id "<< m_level1Id << std::endl);
         m_eventFragment=m_builder->createFragment(m_level1Id,
                                                   m_nChannels);
      }
      else {
         s_mutex->unlock();
         std::this_thread::sleep_for(std::chrono::microseconds(1));

         return REQUEST_NOTFOUND; // Not really but don't have a good 'retry' code
      }
      s_mutex->unlock();
   }

   // Pre-request data from all DataChannels involved
   std::vector<DataChannel*>::iterator chanIter=m_chanStartIter;
   for (std::uint32_t index=0; index<m_nChannels; index++) {
      m_ticket[index]=(*chanIter)->requestFragment(m_level1Id);
      ++chanIter;
   }

   bool fragmentOk=true;
   int partsReceived=0;

   chanIter=m_chanStartIter;
   for (std::uint32_t index=0; index<m_nChannels; index++) {
      EventFragment* subFragment=(*chanIter)->getFragment(m_ticket[index]);
      partsReceived++;
      DEBUG_TEXT(DFDB_ROSCORE, 20, "FragmentRequest::execute calling m_builder->appendFragment for L1Id "<< m_level1Id<< std::endl);
      m_builder->appendFragment(m_eventFragment,subFragment);

      fragmentOk=subFragment->fragmentReady();

//         std::lock_guard<std::mutex> lock(*s_mutex);
      s_mutex->lock();
      delete subFragment;
      s_mutex->unlock();

      ++chanIter;
   }

   bool retired=false;
   if (!fragmentOk) {
      retired=checkAge(s_maxAge);
   }

   if (fragmentOk || (retired && partsReceived>0)) {
      if (retired) {
         // Issue warning message
         CREATE_ROS_EXCEPTION(tIssue,CoreException,CoreException::TIMEOUT,
                              "in request for fragment with L1 ID "<<m_level1Id);
         ers::warning(tIssue);
      }

      m_builder->closeFragment(m_eventFragment);

      std::unique_ptr<RosDataOutputMsg> dataMsg(
         new RosDataOutputMsg(m_transactionId,m_eventFragment,s_mutex));
      m_session->asyncSend(std::move(dataMsg));

      if (retired) {
         return(REQUEST_TIMEOUT);
      }
      return REQUEST_OK;
   }
   else {
     s_mutex->lock();
     delete m_eventFragment;
     s_mutex->unlock();
     return REQUEST_NOTFOUND;
   }
}




std::string DFRequest::what()
{
   std::ostringstream sstream;
   sstream << "DFRequest for event id " << m_level1Id;
   if (m_dataChannels->size()!=0) {
      sstream << ", channels" << std::hex;
      for  (std::vector<DataChannel*>::iterator dc=m_dataChannels->begin();
            dc!=m_dataChannels->end(); ++dc) {
         sstream << " " << (*dc)->id();
      }
      sstream << std::dec;
   }
   else {
      sstream << ", all channels";
   }
   return (sstream.str());
}

std::ostream& DFRequest::put(std::ostream& stream) const
{
   stream << "Fragment Request for event " << m_level1Id ;
   return stream;
}
