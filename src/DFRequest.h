// -*- c++ -*-

#ifndef DFREQUEST_H
#define DFREQUEST_H
#include <mutex>
#include <vector>
#include <memory>

#include "ROSCore/DataRequest.h"

namespace ROS{
   class FragmentBuilder;
   class ROSFragmentBuilder;
   class MEtFragmentBuilder;
   class DataServerSession;
   class DataChannel;
   class EventFragment;
   class DFRequest : public DataRequest {
   public:
//      DFRequest(std::unique_ptr<std::vector<std::uint32_t>> rols,
      DFRequest(std::vector<std::uint32_t>* rols,
                DataServerSession* session,
                std::uint32_t level1Id,
                std::uint32_t transactionId);
      virtual ~DFRequest();

      virtual int execute(void) override;
      virtual std::ostream& put(std::ostream& stream) const override;
      virtual std::string what() override;

      static void configure(unsigned int);

   private:
      static ROSFragmentBuilder* s_rosBuilder;
      static MEtFragmentBuilder* s_metBuilder;
      static unsigned int s_metID;
      static std::mutex s_metMutex;

      std::unique_ptr<std::vector<DataChannel*> > m_dataChannels;
      std::vector<DataChannel*>::iterator m_chanStartIter;
      FragmentBuilder* m_builder;
      unsigned int m_nChannels;

      DataServerSession* m_session;

      const unsigned int m_transactionId;

      virtual bool buildFragment() override {return true;};

   };
} // namespace ros
#endif
