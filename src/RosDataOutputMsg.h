// -*- c++ -*-
//
#ifndef ROS_DATA_OUTPUT_MSG_H
#define ROS_DATA_OUTPUT_MSG_H

#include "ROSasyncmsg/RosDataMsg.h"
#include "ROSEventFragment/EventFragment.h"
#include "ROSBufferManagement/Buffer.h"
#include "DFThreads/DFFastMutex.h"


class RosDataOutputMsg: public RosDataMsg,
                        public daq::asyncmsg::OutputMessage {
public:

   explicit RosDataOutputMsg(std::uint32_t transactionId,
                             ROS::EventFragment* frag, DFFastMutex* mut)
      : RosDataMsg(transactionId),m_fragment(frag),m_mutex(mut) {
   };

   /**
    *  Destructor of the message must delete the event fragment. 
    **/
   virtual ~RosDataOutputMsg(){
      //std::cout << "RosDataOutputMsg destructor\n";
      //std::lock_guard<std::mutex>(*s_mutex);
      if (m_fragment!=0) {
         m_mutex->lock();
         //std::cout << "RosDataOutputMsg destructor got lock, deleting m_fragment\n";
         delete m_fragment;
         m_mutex->unlock();
      }
   };

   virtual std::uint32_t size() const override {
      uint32_t sum=0;
      ROS::Buffer::page_iterator page=m_fragment->buffer()->begin();
      for (page++; page!=m_fragment->buffer()->end(); page++) { //skip over 1st page containing FullFragment header
         sum+=(*page)->usedSize();
      }
      return sum;
   }


   virtual void toBuffers(std::vector<boost::asio::const_buffer>& buffers) const override{
      //std::cout << "RosDataOutputMsg::toBuffers()\n";
      ROS::Buffer::page_iterator page=m_fragment->buffer()->begin();
      for (page++; page!=m_fragment->buffer()->end(); page++) { //skip over 1st page containing FullFragment header
         buffers.emplace_back((*page)->address(),(*page)->usedSize());
      }
   }


   ROS::EventFragment* m_fragment;
   DFFastMutex* m_mutex;
};
#endif
