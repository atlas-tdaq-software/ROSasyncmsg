#include <iostream>

#include "DFTriggerIn.h"
#include "ROSasyncmsg/DataServer.h"
#include "ROSasyncmsg/ClearSession.h"
#include "DFRequest.h"
#include "ROSasyncmsg/DataRequestMsg.h"
#include "ROSasyncmsg/RosDataMsg.h"
#include "ROSasyncmsg/ClearMsg.h"


#include "ROSIO/ReleaseRequest.h"
#include "ROSCore/DataChannel.h"
#include "ROSCore/IOManager.h"
#include "asyncmsg/NameService.h"


#include "dal/Partition.h"
#include "DFdal/DFParameters.h"


namespace ROS {
   class RHDataServer: public DataServer {
   public:
      RHDataServer(std::vector<boost::asio::io_service>& ioService,
                   IOManager* iom,
                   daq::asyncmsg::NameService* nameService);

      virtual ~RHDataServer();
   private:

      virtual void onAccept(std::shared_ptr<daq::asyncmsg::Session>) noexcept;
      IOManager* m_ioManager;
   };


   class RHDataServerSession: public DataServerSession {

   public:
      RHDataServerSession(boost::asio::io_service& ioService,IOManager* iom);
      virtual ~RHDataServerSession();
   private:
      virtual void onReceive(std::unique_ptr<daq::asyncmsg::InputMessage> message);
      IOManager* m_ioManager;
   };

   class RHClearSession: public ClearSession {
   public:
      RHClearSession(boost::asio::io_service& ioService,IOManager* iom,
                     const std::string& mcAddress,
                     const std::string& mcInterface):
         ClearSession(ioService, mcAddress, mcInterface),m_ioManager(iom) {};
      virtual ~RHClearSession(){};
   private:
      virtual void onReceive(std::unique_ptr<daq::asyncmsg::InputMessage> message);
      IOManager* m_ioManager;
   };
}

using namespace ROS;

RHDataServer::RHDataServer(std::vector<boost::asio::io_service>& ioService,
                           IOManager* iom,
                           daq::asyncmsg::NameService* nameService) :
   DataServer(ioService,iom->getName(),nameService),m_ioManager(iom) {
}


RHDataServer::~RHDataServer() {
}
void RHDataServer::onAccept(std::shared_ptr<daq::asyncmsg::Session> session) noexcept {
   {
      std::lock_guard<std::mutex> lock(m_sessionsMutex);
      m_sessions.emplace_back(std::dynamic_pointer_cast<DataServerSession>(session));
   }
//   std::cout << "DataServer::onAccept() " << m_sessions.size() << " sesions created\n";
   int server=getIoService();
   asyncAccept(std::make_shared<RHDataServerSession>(
                  m_ioServerService[server],m_ioManager));

}

RHDataServerSession::RHDataServerSession(boost::asio::io_service& ioService,
                                                         IOManager* iom) :
   DataServerSession(ioService),m_ioManager(iom) {
}
RHDataServerSession::~RHDataServerSession() {
}
void RHDataServerSession::onReceive(std::unique_ptr<daq::asyncmsg::InputMessage> message) {

   if (message->typeId()==DataRequestMsg::TYPE_ID) {
      std::unique_ptr<DataRequestMsg> requestMsg(dynamic_cast<DataRequestMsg*>(message.release()));
      if (requestMsg==0) {
         std::cerr << "Failed to cast to DataRequestMsg\n";
         return;
      }

//       std::cout << std::hex
//                 << "Received request message " << requestMsg->transactionId()
//                 << " for event " << requestMsg->level1Id()
//                 << " fingering " << requestMsg->nRequestedRols() << " ROLs:";
//       std::cout << std::dec << std::endl;

      DFRequest* request=new DFRequest(
         requestMsg->requestedRols(),
         this,
         requestMsg->level1Id(),
         requestMsg->transactionId());

      //std::cout << "Queueing request with IOManager\n";
      //std::lock_guard<std::mutex> lock(s_mutex);
      m_ioManager->queueRequest(request);
   }
   else if (message->typeId()==ClearMsg::TYPE_ID) {
      std::unique_ptr<ClearMsg> clearMsg(dynamic_cast<ClearMsg*>(message.release()));
      if (clearMsg!=0) {
         //std::cout << "Received clear message for " << clearMsg->nEvents() << " events\n";
         ReleaseRequest* request=new ReleaseRequest(clearMsg->m_l1Ids,
                                                    DataChannel::channels());
         m_ioManager->queueRequest(request);
      }
      else
         std::cerr << "failed to cast to ClearMsg\n";
   }
   else 
      std::cout << "WTF type of message was that?\n";

   asyncReceive();
}



void RHClearSession::onReceive(std::unique_ptr<daq::asyncmsg::InputMessage> message) {

   // We might as well start receiving the next one while we process this one
   asyncReceive();

   if (message->typeId()==ClearMsg::TYPE_ID) {
      std::unique_ptr<ClearMsg> clearMsg(dynamic_cast<ClearMsg*>(message.release()));
      if (clearMsg!=0) {
         //std::cout << "Received clear message for " << clearMsg->nEvents() << " events\n";
         updateStatistics(clearMsg->transactionId(),clearMsg->nEvents());
         ReleaseRequest* request=new ReleaseRequest(clearMsg->m_l1Ids,
                                                    DataChannel::channels());
         m_ioManager->queueRequest(request);
      }
      else
         std::cerr << "failed to cast to ClearMsg\n";
   }
   else 
      std::cout << "WTF type of message was that?\n";
}




DFTriggerIn::DFTriggerIn(): m_nameService(0),
                            m_nDataServerThreads(1),
                            m_standalone(false) {
   std::cout << "DFTriggerIn::DFTriggerIn()\n";
   resetCounters();
}

DFTriggerIn::~DFTriggerIn() noexcept {
   std::cout << "DFTriggerIn::~DFTriggerIn()\n";
}


void DFTriggerIn::setup(ROS::IOManager* iom, DFCountedPointer<ROS::Config> config){
//    std::cout << "\n\nDFTriggerIn\n";
//    config->dump();

   m_ioManager=iom;
   m_nDataServerThreads=config->getInt("nDataServerThreads");

   string partitionName=config->getString("partition");
   m_ipcPartition=IPCPartition(partitionName.c_str());
   m_standalone=config->isKey("standalone");

   unsigned int detectorId;
   detectorId=config->getUInt("SubDetectorId");
   DFRequest::configure(detectorId);


   std::cout << "DFTriggerIn::setup()  UID=" << m_uid
                << ",  m_partition=" << std::hex << m_partition << std::dec << std::endl;

}

void DFTriggerIn::configure(const daq::rc::TransitionCmd&) {
   std::cout << "DFTriggerIn::configure()  UID=" << m_uid
                << ",  m_partition=" << std::hex << m_partition << std::dec << std::endl;
   resetCounters();
   const daq::df::DFParameters* dfPars=
      m_configurationDB->cast<daq::df::DFParameters>(m_partition->get_DataFlowParameters());

   if (!m_standalone) {
      // Try and get networks from OKS
      std::vector<std::string> networks=dfPars->get_DefaultDataNetworks();
//       std::cout << "Network list:\n";
//       for (auto niter=networks.begin(); niter!=networks.end(); niter++) {
//          std::cout << (*niter) << std::endl;
//       }
//       std::cout << "DFTriggerIn::configure() Creating NameService\n";
      m_nameService=new daq::asyncmsg::NameService(m_ipcPartition,
                                                   networks);
   }

   std::cout << "DFTriggerIn::configure() opening data server\n";
   m_dataIoServices.reset(new std::vector<boost::asio::io_service>(m_nDataServerThreads));
   m_dataServer=std::make_shared<RHDataServer>(*m_dataIoServices,
                                               m_ioManager,
                                               m_nameService);
   m_dataServer->configure(1);
   //------>>
   int service=m_dataServer->getIoService();
   m_dataServer->asyncAccept(std::make_shared<RHDataServerSession>(
                                (*m_dataIoServices)[service],
                                m_ioManager));
   //------>>



   std::string mcAddress=dfPars->get_MulticastAddress();
   std::string proto=mcAddress.substr(0,mcAddress.find(":"));
   if (!proto.empty()) {
      mcAddress=mcAddress.substr(mcAddress.find(":")+1);
   }
   if (mcAddress.empty() || proto=="tcp") {
      // use TCP
      m_clearIoService.reset(new std::vector<boost::asio::io_service>(1));
      m_clearServer=std::make_shared<RHDataServer>(*m_clearIoService,
                                                   m_ioManager,
                                                   m_nameService);
      m_clearServer->configure(1,"CLEAR_");
   //------>>
      m_clearServer->asyncAccept(std::make_shared<RHDataServerSession>((*m_clearIoService)[0],m_ioManager));
   //------>>

   }
   else {
      auto sPos=mcAddress.find("/");
      std::string mcInterface;
      if (sPos!=std::string::npos) {
         mcInterface=mcAddress.substr(sPos+1);
      }
      mcAddress=mcAddress.substr(0,sPos);
      m_clearIoService.reset(new std::vector<boost::asio::io_service>(m_nDataServerThreads));
      m_clearSession=std::make_shared<RHClearSession>((*m_clearIoService)[0],
                                                    m_ioManager,
                                                    mcAddress,
                                                    mcInterface);
      m_clearSession->asyncReceive();
   }
}


void DFTriggerIn::unconfigure(const daq::rc::TransitionCmd&) {
   std::cout << "DFTriggerIn::unconfigure() closing data server\n";
   m_dataServer->unconfigure();

   if (m_clearServer) {
      m_clearServer->unconfigure();
   }

   if (m_clearSession) {
      m_clearSession->unconfigure();
   }

   if (m_nameService!=0) {
      delete m_nameService;
      m_nameService=0;
   }
}

void DFTriggerIn::resetCounters() {
   m_stats.numberOfLevel1=0;
   m_stats.l2RequestsQueued=0;
   m_stats.ebRequestsQueued=0;
   m_stats.clearRequestsQueued=0;
   m_stats.gcRequestsQueued=0;
   m_stats.clearRequestsLost=0;
}

ISInfo* DFTriggerIn::getISInfo() {
   if (m_clearSession) {
      ClearSessionStats* clrStats=m_clearSession->getStats();
      m_stats.clearRequestsQueued=clrStats->clearRequestsReceived;
      m_stats.clearRequestsLost=clrStats->clearRequestsLost;
      m_stats.numberOfLevel1=clrStats->clearsRequested;
   }
   return &m_stats;
}


extern "C" {
   extern TriggerIn* createDFTriggerIn();
}
TriggerIn* createDFTriggerIn()
{
   return (new DFTriggerIn());
}
