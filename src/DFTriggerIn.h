// -*- c++ -*-
// $Id:$ 
//
// $Log:$
//
#ifndef DFTRIGGER_IN_H
#define DFTRIGGER_IN_H

#include "ROSasyncmsg/DataServer.h"
#include "ROSasyncmsg/ClearSession.h"

#include "ROSCore/TriggerIn.h"
#include "ROSInfo/DcTriggerInInfo.h"
#include "dal/Partition.h"

namespace daq{
   namespace asyncmsg{
      class NameService;
   }
}
namespace ROS{
   //! ROS DataFlow Trigger

   //! Listen for 'requests' from DataFlow for data or clears
   class DFTriggerIn : public TriggerIn {
   public:
      DFTriggerIn();
      virtual ~DFTriggerIn() noexcept;

      void configure(const daq::rc::TransitionCmd&) override;
      void unconfigure(const daq::rc::TransitionCmd&) override;

      ISInfo* getISInfo() override;
   private:
      //! From DFThread not needed
      virtual void run() override {};
      //! From DFThread not needed
      virtual void cleanup() override {};

      //! From IOMPlugin
      virtual void setup(ROS::IOManager*, DFCountedPointer<ROS::Config>) override;


      void resetCounters();

      //
      std::unique_ptr<std::vector<boost::asio::io_service> > m_dataIoServices;
      std::unique_ptr<std::vector<boost::asio::io_service> >m_clearIoService;

      //! Server to field data requests, inprinciple we should
      //! have a ClearServer as well but for the moment the DataServer handles
      //! clears as well
      std::shared_ptr<DataServer> m_dataServer;
      std::shared_ptr<DataServer> m_clearServer;
      std::shared_ptr<ClearSession> m_clearSession;

      daq::asyncmsg::NameService* m_nameService;

      int m_nDataServerThreads;
      IPCPartition m_ipcPartition;
      bool m_standalone;

      DcTriggerInInfo m_stats;
   };
}
#endif
