#include <thread>
#include <chrono>
#include <iostream>

#include "ROSasyncmsg/ClearSession.h"
#include "ROSasyncmsg/ClearMsg.h"

#include "ers/ers.h"

#include "asyncmsg/NameService.h"


using namespace daq;
using namespace ROS;

ERS_DECLARE_ISSUE(ROSasyncmsg,
		  ClearSessionError,
		  "" << reason << " (addr: " 
		  << remoteEndpoint << "). "
		  << action << ". ",
		  ((std::string) reason)
		  ((std::string) remoteEndpoint)
		  ((std::string) action))

namespace KeepAliveMsg {
  static const std::uint32_t TYPE_ID=0x00DCDF11;
}

ClearSession::ClearSession(boost::asio::io_service& ioService,
                           const std::string& mcAddress,
                           const std::string& mcInterface) :
   asyncmsg::UDPSession(ioService,9000),

   m_lastClearXid(0) {
   //std::cout << "Configuring ClearSession for multicast: " << mcAddress << '/' << mcInterface << std::endl;
   joinMulticastGroup(boost::asio::ip::address::from_string(mcAddress),
                      daq::asyncmsg::NameService::find_interface(mcInterface));

   m_ioServiceWork.reset(new boost::asio::io_service::work(ioService));
   m_ioServiceThread.reset(new boost::thread(
                              boost::bind(&boost::asio::io_service::run,
                                          &ioService)));

   m_stats.clearRequestsLost=0;
   m_stats.clearRequestsReceived=0;
   m_stats.clearsRequested=0;
}

ClearSession::~ClearSession() {
}

void ClearSession::unconfigure() {
   std::cout << "ClearSession::unconfigure()\n";
   close();
   m_ioServiceWork.reset(); // This should tell io_service to stop!

   m_ioServiceThread->join();
   m_ioServiceThread.reset();
   std::cout << "ClearSession::unconfigure() complete\n";
}




ClearSessionStats* ClearSession::getStats() {
   return &m_stats;
}

void ClearSession::updateStatistics(unsigned int xid, unsigned int nEvents) {
   if (m_lastClearXid != 0 && (xid - m_lastClearXid) > 1) {
      m_stats.clearRequestsLost+=(xid-m_lastClearXid)-1;
   }
   m_lastClearXid=xid;
   m_stats.clearRequestsReceived++;
   m_stats.clearsRequested+=nEvents;
}

std::unique_ptr<asyncmsg::InputMessage>
ClearSession::createMessage(std::uint32_t typeId,
                            std::uint32_t transactionId,
                            std::uint32_t size) noexcept{
   if (typeId == ClearMsg::TYPE_ID) {
      std::unique_ptr<asyncmsg::InputMessage> message;
      try {
         message.reset(new ClearMsg(size,transactionId));
      }
      catch (std::exception& ex) {
	std::ostringstream endpoint;
	endpoint << remoteEndpoint();

	std::ostringstream errorMessage;
	errorMessage << "Cannot create clear message. Size: " 
		<< size << std::dec << " ";
	
	ers::warning(ROSasyncmsg::ClearSessionError(ERS_HERE, 
						    errorMessage.str(),
						    endpoint.str(), 
						    "Ignoring", ex));
	asyncReceive();
	return std::unique_ptr<asyncmsg::InputMessage>();
      }
      return message;
   }
   else if (typeId != KeepAliveMsg::TYPE_ID){
     std::ostringstream endpoint;
     endpoint << remoteEndpoint();
     
     std::ostringstream message;
     message << "Unknown message received. Type: " 
	     << std::hex << typeId << std::dec << " ";

     ers::warning(ROSasyncmsg::ClearSessionError(ERS_HERE, 
						 message.str(),
						 endpoint.str(), 
						 "Ignoring"));
   }

   // We have nothing to do in this case
   // just call asyncReceive to keep the wheels turning
   asyncReceive();
   return std::unique_ptr<asyncmsg::InputMessage>();
}


void ClearSession::onReceiveError(const boost::system::error_code& error,
                                       std::unique_ptr<asyncmsg::InputMessage> message) noexcept{
   if (error) {
      if (error!=boost::asio::error::eof && error!=boost::asio::error::operation_aborted) {
         if (message) {
            ERS_LOG("Error receiving message type " << message->typeId()
                    << " from " << remoteEndpoint() << ": "
                    << error.message() << " (" << error << "). Aborting.");
         }
         else {
            ERS_LOG("Error receiving message -- message not set -- "
                    << " from " << remoteEndpoint() << ": "
                    << error.message() << " (" << error << "). Aborting.");

         }
         close();
      }
//       else {
//          std::cout << remoteEndpoint() << " shut connection\n";
//       }
   }
}

void ClearSession::onSend(std::unique_ptr<const asyncmsg::OutputMessage> ) noexcept{
}

void ClearSession::onSendError(const boost::system::error_code& , std::unique_ptr<const asyncmsg::OutputMessage>) noexcept{
}
