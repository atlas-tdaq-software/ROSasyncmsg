#include <thread>
#include <chrono>
#include <iostream>

#include "ROSasyncmsg/DataServer.h"
#include "ROSasyncmsg/DataRequestMsg.h"
#include "ROSasyncmsg/ClearMsg.h"

#include "ers/ers.h"


#include "asyncmsg/Session.h"
#include "asyncmsg/NameService.h"


using namespace daq;
using namespace ROS;

DataServer::DataServer(std::vector<boost::asio::io_service>& ioService,
                       const std::string& myName, asyncmsg::NameService* nameService) :
   asyncmsg::Server(ioService[0]),
   m_ioServerService(ioService),
   m_nextIoService(0),
   m_name(myName),
   m_nameService(nameService) {
}

int DataServer::getIoService() {
   m_nextIoService++;
   if (m_nextIoService==m_ioServerService.size()) {
      m_nextIoService=0;
   }
   return m_nextIoService;
}

DataServer::~DataServer() {
}


void DataServer::configure(int nThreads, const std::string& tag) {
//    for (int threadCount=0; threadCount<nThreads; threadCount++) {

//       std::unique_ptr<boost::asio::io_service::work> work(
//          new boost::asio::io_service::work(m_ioServerService[threadCount]));
//       m_ioServiceWork.push_back(std::move(work));
//       std::unique_ptr<boost::thread> thread(
//          new boost::thread(boost::bind(&boost::asio::io_service::run,
//                                        &m_ioServerService[threadCount])));
//       m_ioServiceThreads.push_back(std::move(thread));
//    }
   std::string threadName="DataServer";
   if (tag!="") {
      threadName=tag+"Server";
   }
   for (unsigned int serviceCount=0; serviceCount<m_ioServerService.size(); serviceCount++) {
      std::unique_ptr<boost::asio::io_service::work> work(
         new boost::asio::io_service::work(m_ioServerService[serviceCount]));
      m_ioServiceWork.push_back(std::move(work));

      for (int threadCount=0; threadCount<nThreads; threadCount++) {
         std::unique_ptr<boost::thread> thread(
            new boost::thread(boost::bind(&boost::asio::io_service::run,
                                          &m_ioServerService[serviceCount])));


         pthread_setname_np(thread->native_handle(),threadName.c_str());
         m_ioServiceThreads.push_back(std::move(thread));
      }
   }
   std::cout << "DataServer::configure() Created " << m_ioServiceThreads.size() << " io threads\n";

   std::uint16_t port=0;

   if (m_nameService==0) {
      // For interactive testing withou IS, always listen on 9000
      port=9000;
      if (tag!="") {
         // Or 9001  for clears
         port++;
      }
   }


   listen("ROS", 
          boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(),
                                         port)
      );


   if (m_nameService!=0) {
      boost::asio::ip::tcp::endpoint ep=localEndpoint();
      std::string name=tag+m_name;
//       std::cout << "DataServer::configure() publishing endpoint port " <<
//          ep.port() << " with name " << name << std::endl;
      m_nameService->publish(name,ep.port());
   }
}

void DataServer::unconfigure() {
   for (unsigned int service=0; service<m_ioServiceWork.size(); service++) {
      m_ioServiceWork[service].reset();
   }

//   m_ioService.stop();  // But it doesn't, so stop it explicitely!!
   close();

   std::lock_guard<std::mutex> lock(m_sessionsMutex);
   std::cout << "DataServer::unconfigure() " << m_sessions.size() << " sessions in vector\n";
   int tries=0;
   while (!m_sessions.empty() && tries<2) {
      auto it = m_sessions.begin();
      while (it != m_sessions.end()) {
         if (auto session = it->lock()) {
            session->asyncClose();
            it++;
         }
         else {
            //std::cout << "Pointer expired erasing\n";
            it = m_sessions.erase(it);
         }
      }
      if (!m_sessions.empty()) {
         m_sessionsMutex.unlock();
         std::cout << "DataServer::unconfigure " << m_sessions.size() << " connections still active sleeping\n";
         std::this_thread::sleep_for(std::chrono::seconds(1));
         m_sessionsMutex.lock();
      }
      tries++;
   }


   for (unsigned int num=0; num<m_ioServiceThreads.size(); num++){
      m_ioServiceThreads[num]->join();
   }
   while (m_ioServiceThreads.size()!=0) {
      m_ioServiceThreads.pop_back();
   }
}




void DataServer::onAcceptError(const boost::system::error_code& error,
                               std::shared_ptr<asyncmsg::Session> /* session */) noexcept{
   std::cout << "DataServer::onAcceptError()\n";
   if (error) {
      if (error == boost::asio::error::operation_aborted) {
         // ignore the accept cancellation that happens when closing the Server
         return;
      }
      else {
         ERS_LOG("An error occurred while accepting a connection. Ignoring." << error);
      }
   }
}


std::mutex DataServerSession::s_mutex;

DataServerSession::DataServerSession(boost::asio::io_service& ioService) :
   asyncmsg::Session(ioService),m_open(false) {
   //std::cout << "DataServerSession::DataServerSession()\n";
}

DataServerSession::~DataServerSession() {
   //std::cout << "DataServerSession::~DataServerSession()\n";
}

std::unique_ptr<asyncmsg::InputMessage> DataServerSession::createMessage(std::uint32_t typeId,
                                                               std::uint32_t transactionId,
                                                               std::uint32_t size) noexcept{
  if (typeId == DataRequestMsg::TYPE_ID) {
    std::unique_ptr<asyncmsg::InputMessage> message;
    try {
       message.reset(new DataRequestMsg(size,transactionId));
    }
    catch (std::exception& ex) {
      ERS_LOG("Unexpected message size " << size << " from " << remoteEndpoint() << ". Ignoring.");
      return std::unique_ptr<asyncmsg::InputMessage>();
    }
    return message;
  }
  else if (typeId == ClearMsg::TYPE_ID) {
    std::unique_ptr<asyncmsg::InputMessage> message;
    try {
       message.reset(new ClearMsg(size,transactionId));
    }
    catch (std::exception& ex) {
      ERS_LOG("Unexpected message size " << size << " from " << remoteEndpoint() << ". Ignoring.");
      return std::unique_ptr<asyncmsg::InputMessage>();
    }
    return message;
  }
  else {
     ERS_LOG("Unexpected message type 0x" << std::hex << typeId << std::dec << " from " << remoteEndpoint() << ". Ignoring.");
    return std::unique_ptr<asyncmsg::InputMessage>();
  }
}



void DataServerSession::onOpen() noexcept{
   m_open=true;
   asyncReceive();
}

void DataServerSession::onOpenError(const boost::system::error_code& error) noexcept{
   ERS_LOG("Error opening session with peer " << remoteEndpoint() << ": " <<
           error.message() << " (" << error << "). Aborting.");
}

void DataServerSession::onClose() noexcept{
   m_open=false;
}

void DataServerSession::onCloseError(const boost::system::error_code& error) noexcept{
   ERS_LOG("Error closing session with peer " << remoteEndpoint() << ": " <<
           error.message() << " (" << error << "). Ignoring.");
 }

void DataServerSession::onReceiveError(const boost::system::error_code& error,
                                       std::unique_ptr<asyncmsg::InputMessage> message) noexcept{
   if (error) {
      if (error!=boost::asio::error::eof && error!=boost::asio::error::operation_aborted) {
         if (message) {
            ERS_LOG("Error receiving message type " << message->typeId()
                    << " from " << remoteEndpoint() << ": "
                    << error.message() << " (" << error << "). Aborting.");
         }
         else {
            ERS_LOG("Error receiving message -- message not set -- "
                    << " from " << remoteEndpoint() << ": "
                    << error.message() << " (" << error << "). Aborting.");

         }
      }
//       else {
//          std::cout << remoteEndpoint() << " shut connection\n";
//       }
   }
   if (m_open) {
      ERS_LOG("DataServerSession::onReceiveError() Closing");
      asyncClose();
      m_open=false;
   }
}

void DataServerSession::onSend(std::unique_ptr<const asyncmsg::OutputMessage>  /*message*/) noexcept{
   //std::cout << "Sent message " << message->transactionId() << std::endl;

}

void DataServerSession::onSendError(const boost::system::error_code& error, std::unique_ptr<const asyncmsg::OutputMessage> message) noexcept{
   ERS_LOG("Error sending message type " << message->typeId() << " to " << remoteEndpoint() << ": " <<
           error.message() << " (" << error << "). Aborting.");
   if (m_open) {
      asyncClose();
      m_open=false;
   }
}
