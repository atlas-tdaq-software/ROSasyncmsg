// -*- c++ -*-
//
#ifndef CLEARSESSION_H
#define CLEARSESSION_H

#include <mutex>
#include <string>
#include <boost/thread/thread.hpp>
#include "asyncmsg/UDPSession.h"

namespace ROS{

   class ClearSessionStats {
   public:
      unsigned int clearRequestsLost;
      unsigned int clearRequestsReceived;
      unsigned int clearsRequested;
   };

   class ClearSession: public daq::asyncmsg::UDPSession {

   public:
      ClearSession(boost::asio::io_service& ioService,
                   const std::string& mcAddress,
                   const std::string& mcInterface);
      virtual ~ClearSession();

      void unconfigure();
      ClearSessionStats* getStats();
   private:
      virtual std::unique_ptr<daq::asyncmsg::InputMessage> createMessage(
         std::uint32_t typeId, std::uint32_t transactionId,
         std::uint32_t size) noexcept;
      //virtual void onReceive(std::unique_ptr<daq::asyncmsg::InputMessage> message);
      //virtual void onReceive(std::uint32_t typeId, std::uint32_t transactionId) noexcept;
      virtual void onReceiveError(const boost::system::error_code& error, std::unique_ptr<daq::asyncmsg::InputMessage> message) noexcept;
      virtual void onSend(std::unique_ptr<const daq::asyncmsg::OutputMessage> message) noexcept;
      virtual void onSendError(const boost::system::error_code& error, std::unique_ptr<const daq::asyncmsg::OutputMessage> message) noexcept;

      std::unique_ptr<boost::thread> m_ioServiceThread;
      std::unique_ptr<boost::asio::io_service::work> m_ioServiceWork;

   protected:
      void updateStatistics(unsigned int xid, unsigned int nEvents);
      std::string m_name;
      unsigned int m_lastClearXid;

      ClearSessionStats m_stats;
   };
}
#endif
