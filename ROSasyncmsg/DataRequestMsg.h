// -*- c++ -*-
//
#ifndef DATA_REQUEST_MSG_H
#define DATA_REQUEST_MSG_H

#include "asyncmsg/Message.h"

namespace ROS {
   class DataRequestMsg: public daq::asyncmsg::OutputMessage,
                         public daq::asyncmsg::InputMessage {
   public:

      static const std::uint32_t TYPE_ID=0x00DCDF20;


      explicit DataRequestMsg(std::uint32_t messageSize, std::uint32_t transactionId) :
         m_transactionId(transactionId),m_level1Id(0) {
         auto nRols=messageSize/sizeof(std::uint32_t)-1;
         m_rols.resize(nRols);
      };

      explicit DataRequestMsg(std::vector<std::uint32_t>& rols, std::uint32_t l1Id, std::uint32_t transactionId) :
         m_transactionId(transactionId),m_level1Id(l1Id),m_rols(rols) {
      };

      std::uint32_t typeId() const override;
      std::uint32_t transactionId() const override;
      std::uint32_t level1Id() const;
      void level1Id(std::uint32_t lvl1Id);
      std::uint32_t nRequestedRols() const;
//      std::unique_ptr<std::vector<std::uint32_t> > requestedRols();
      std::vector<std::uint32_t>* requestedRols();
      void addRol(std::uint32_t rol);

      virtual void toBuffers(std::vector<boost::asio::const_buffer>& buffers) const override;
      virtual void toBuffers(std::vector<boost::asio::mutable_buffer>& buffers) override;

   private:
      std::uint32_t m_transactionId;
      std::uint32_t m_level1Id;

//      std::unique_ptr<std::vector<std::uint32_t>> m_rols;
      std::vector<std::uint32_t> m_rols;
   };

   inline void DataRequestMsg::toBuffers(std::vector<boost::asio::const_buffer>& buffers) const {
      buffers.emplace_back(static_cast<const void*>(&m_level1Id), sizeof(m_level1Id));
//      buffers.emplace_back(m_rols->data(), m_rols->size()*sizeof(std::uint32_t));
      buffers.emplace_back(m_rols.data(), m_rols.size()*sizeof(std::uint32_t));
   }

   inline void DataRequestMsg::toBuffers(std::vector<boost::asio::mutable_buffer>& buffers) {
      buffers.emplace_back(static_cast<void*>(&m_level1Id), sizeof(m_level1Id));
//      buffers.emplace_back(m_rols->data(), m_rols->size()*sizeof(std::uint32_t));
      buffers.emplace_back(m_rols.data(), m_rols.size()*sizeof(std::uint32_t));
   }


   inline std::uint32_t DataRequestMsg::typeId() const {
      return TYPE_ID;
   }
   inline std::uint32_t DataRequestMsg::transactionId() const {
      return m_transactionId;
   }
   inline std::uint32_t DataRequestMsg::level1Id() const {
      return m_level1Id;
   }
   inline void DataRequestMsg::level1Id(std::uint32_t lvl1Id){
      m_level1Id=lvl1Id;
   }
   inline std::uint32_t DataRequestMsg::nRequestedRols() const {
//      return m_rols->size();
      return m_rols.size();
   }
//    inline std::unique_ptr<std::vector<std::uint32_t> > DataRequestMsg::requestedRols(){
//       return std::move(m_rols);
//    }
   inline std::vector<std::uint32_t>* DataRequestMsg::requestedRols(){
      return &m_rols;
   }
   inline void DataRequestMsg::addRol(std::uint32_t rol) {
//      m_rols->push_back(rol);
      m_rols.push_back(rol);
   }
}
#endif
