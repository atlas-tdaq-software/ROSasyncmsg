// -*- c++ -*-
//
#ifndef CLEAR_MSG_H
#define CLEAR_MSG_H

#include "asyncmsg/Message.h"


class ClearMsg: public daq::asyncmsg::InputMessage,
                public daq::asyncmsg::OutputMessage {
public:

   static const std::uint32_t TYPE_ID=0x00DCDF10;

   std::vector<std::uint32_t> m_l1Ids;

   explicit ClearMsg(std::uint32_t messageSize, std::uint32_t transactionId) :
      m_transactionId(transactionId) {
      std::uint32_t numEvents=messageSize/sizeof(std::uint32_t);
      m_l1Ids.resize(numEvents);
//       std::cout << "ClearMsg:: constructed with size" << messageSize <<" (numEvents=" << numEvents << ")\n";
   };

   std::uint32_t typeId() const override {return TYPE_ID;};
   std::uint32_t transactionId() const override {return m_transactionId;};
   std::uint32_t nEvents() const {return m_l1Ids.size();};

  virtual void toBuffers(std::vector<boost::asio::const_buffer>& buffers) const override
  {
    buffers.emplace_back(m_l1Ids.data(), m_l1Ids.size()*sizeof(std::uint32_t));
  }

  virtual void toBuffers(std::vector<boost::asio::mutable_buffer>& buffers) override
  {
    buffers.emplace_back(m_l1Ids.data(), m_l1Ids.size()*sizeof(std::uint32_t));
  }


private:
   std::uint32_t m_transactionId;
};
#endif
