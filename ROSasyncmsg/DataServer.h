// -*- c++ -*-
//
#ifndef DATASERVER_H
#define DATASERVER_H

#include <mutex>
#include <string>
#include <boost/thread/thread.hpp>
#include "asyncmsg/Server.h"
#include "asyncmsg/Session.h"
#include "asyncmsg/NameService.h"


namespace ROS{
   class DataServerSession;

   //! ROS data server

   //!  Class to handle asyncmsg sessions from HLT (DCM).
   //! Listen for connections from HLT and create DataServerSessions.
   class DataServer : public daq::asyncmsg::Server {
   public:
      DataServer(std::vector<boost::asio::io_service>& ioService,
                 const std::string& myName,
                 daq::asyncmsg::NameService* nameService);
      virtual ~DataServer();
      void configure(int nThreads, const std::string& tag="");
      void unconfigure();
      int getIoService();
   private:

//       virtual void onAccept(std::shared_ptr<daq::asyncmsg::Session>) noexcept;
      virtual void onAcceptError(const boost::system::error_code& error,
                                 std::shared_ptr<daq::asyncmsg::Session>) noexcept;

   protected:
      std::list<std::weak_ptr<DataServerSession> > m_sessions;
      std::vector<std::unique_ptr<boost::thread> > m_ioServiceThreads;
      std::vector<boost::asio::io_service>& m_ioServerService;
      std::vector<std::unique_ptr<boost::asio::io_service::work> > m_ioServiceWork;
      unsigned int m_nextIoService;

      std::mutex m_sessionsMutex;
      const std::string m_name;
      daq::asyncmsg::NameService* m_nameService;
      IPCPartition* m_partition;
   };


   //! ROS data server session

   //!  Session class representin a single TCP connection from HLT.
   //! Recieve data requests messages and return datato requester.
   class DataServerSession: public daq::asyncmsg::Session {

   public:
      explicit DataServerSession(boost::asio::io_service& ioService);
      virtual ~DataServerSession();
   private:
      virtual std::unique_ptr<daq::asyncmsg::InputMessage> createMessage(
         std::uint32_t typeId, std::uint32_t transactionId,
         std::uint32_t size) noexcept override;
      virtual void onOpen() noexcept override;
      virtual void onOpenError(const boost::system::error_code& error) noexcept override;
      virtual void onClose() noexcept override;
      virtual void onCloseError(const boost::system::error_code& error) noexcept override;
//       virtual void onReceive(std::unique_ptr<daq::asyncmsg::InputMessage> message);
      virtual void onReceiveError(const boost::system::error_code& error, std::unique_ptr<daq::asyncmsg::InputMessage> message) noexcept override;
      virtual void onSend(std::unique_ptr<const daq::asyncmsg::OutputMessage> message) noexcept override;
      virtual void onSendError(const boost::system::error_code& error, std::unique_ptr<const daq::asyncmsg::OutputMessage> message) noexcept override;

   protected:
      static std::mutex s_mutex;
      bool m_open;
   };
}
#endif
