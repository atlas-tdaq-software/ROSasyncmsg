// -*- c++ -*-
//
#ifndef ROS_DATA_MSG_H
#define ROS_DATA_MSG_H

#include "asyncmsg/Message.h"

class RosDataMsg: public virtual daq::asyncmsg::Message{
public:

   // Where should message types be defined?
   static const std::uint32_t TYPE_ID=0x00DCDF21;


   explicit RosDataMsg(std::uint32_t transactionId) : m_transactionId(transactionId) {
   };
   std::uint32_t typeId() const override {return TYPE_ID;};

   std::uint32_t transactionId() const override {return m_transactionId;};

   virtual std::uint32_t size() const =0;

protected:
   std::uint32_t m_transactionId;
};
#endif
